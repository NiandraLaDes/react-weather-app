# Project Title

React Weather App

# Description

Build a functioning Weather app using the Open Weather API. Tutorial by [Tyler McGinnis](https://tylermcginnis.com/)
The end result of this project can be found [HERE](https://artist-submarine-48713.netlify.com/)

## Built With

* [React.js](https://reactjs.org/) - A JavaScript library for building user interfaces
* [React Router](https://github.com/ReactTraining/react-router) - Declarative routing for React


## Authors

* **Bart van Enter** - *Initial work* - [Bitbucket](https://bitbucket.org/NiandraLaDes) and [Enter Webdevelopment](https://enter-webdevelopment.nl/)

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details