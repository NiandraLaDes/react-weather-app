import React from 'react'
import {browserHistory, IndexRoute, Route, Router} from 'react-router'
import App from '../components/App'
import Home from '../components/Home'
import ForecastContainer from '../containers/ForecastContainer'

var routes = (
    <Router history={browserHistory}>
        <Route path="/" component={App}>
            <IndexRoute component={Home} />
            <Route path="forecast" component={ForecastContainer} />
        </Route>
    </Router>
)

export default routes