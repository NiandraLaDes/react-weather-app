import React, {Component, PropTypes} from 'react';
import Form from '../components/Form'

class FormContainer extends Component {
    constructor(props) {
        super(props)
        this.state = {
            value: ''
        }
    }

    handleSubmit(e) {
        e.preventDefault()
        this.context.router.push({
            pathname: '/forecast',
            query: {city: this.state.value}
        })
    }

    handleChange(e) {
        console.log(e.target.value)
        this.setState({value: e.target.value})
    }

    render() {
        return (
            <Form
                placeholder="Warmenhuizen"
                buttonText="Get Weather"
                value={this.state.value}
                onSubmit={this.handleSubmit.bind(this)}
                onChange={this.handleChange.bind(this)}
            />
        );
    }
}

FormContainer.contextTypes = {
    router: PropTypes.object.isRequired
}

export default FormContainer;
