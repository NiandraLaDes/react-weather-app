import React, { Component } from 'react';
import Forecast from '../components/Forecast'
import apiHelpers from '../utils/apiHelpers'

class ForecastContainer extends Component {
    constructor(props) {
        super(props)
        this.state = {
            isLoading: true,
            forecast: {}
        }
    }

    componentDidMount(){
        const city = this.props.location.query.city;
        apiHelpers.getFiveDayForecast(city)
            .then((data) => {
                this.setState({
                    isLoading: false,
                    forecast: data.data
                })
            })
            .catch((err) => {
                console.log('errors', err)
            })
    }

    render() {
        return (
            <div>
                <Forecast
                    isLoading={this.state.isLoading}
                    forecast={this.state.forecast}
                    city={this.props.location.query.city}
                />
            </div>
        );
    }
}

ForecastContainer.propTypes = {}

export default ForecastContainer;
