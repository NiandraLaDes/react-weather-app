import React, {PropTypes} from 'react'
import dateHelpers from '../utils/dateHelpers'

function puke(obj){
    return <pre>{JSON.stringify(obj, null, ' ')}</pre>
}

const Day = props => {
    const forecast = props.forecast
    const weather = props.forecast.weather[0]

    return (
        <li>
            <h2>{dateHelpers.getDate(forecast.dt)}</h2>
            {puke(weather)}
        </li>
    )
}

Day.propTypes = {
    forecast: PropTypes.object.isRequired
}

export default Day
