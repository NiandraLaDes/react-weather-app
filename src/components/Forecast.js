import React, {PropTypes} from 'react';
import Loading from './Loading'
import Day from './Day'

const Forecast = props => {

    return props.isLoading === true ?
        <Loading /> :
        <div>
            <h1>Weather Forecast {props.city}</h1>
            <ul>
            {props.forecast.list.map((day) => {
                return <Day forecast={day} key={day.dt}/>
            })}
            </ul>
        </div>
}

Forecast.propTypes = {
    forecast: PropTypes.object.isRequired,
    isLoading: PropTypes.bool.isRequired
}
export default Forecast
