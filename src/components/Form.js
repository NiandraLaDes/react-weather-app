import React, {PropTypes} from 'react';
import { Button } from 'react-bootstrap'

const Form = props => (
    <div>
        <form onSubmit={props.onSubmit}>
            <input type="text" placeholder={props.placeholder} onChange={props.onChange} value={props.value}/>
            <Button bsStyle="success" type="submit">{props.buttonText}</Button>
        </form>
    </div>
);

Form.propTypes = {
    placeholder: PropTypes.string.isRequired,
    buttonText: PropTypes.string.isRequired,
    onChange: PropTypes.func.isRequired,
    onSubmit: PropTypes.func.isRequired,
}

export default Form;
