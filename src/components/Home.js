import React from 'react';
import FormContainer from '../containers/FormContainer'

const Home = props => (
    <div>
        <h1>Enter a City and State</h1>
        <FormContainer />
    </div>
);

export default Home;
