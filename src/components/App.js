import React, {Component, PropTypes} from 'react';
import FormContainer from '../containers/FormContainer'
import '../App.css';

class App extends Component {
    render() {
        return (
            <div className="App">
                <div className="App-header">
                    <h2>Weather App</h2>
                    <FormContainer />
                </div>
                <div className="App-main">
                    {this.props.children}
                </div>
            </div>
        );
    }
}

App.propTypes = {
    children: PropTypes.object.isRequired
}

export default App;
