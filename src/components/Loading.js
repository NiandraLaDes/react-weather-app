import React, { Component, PropTypes } from 'react'

class Loading extends Component{
    constructor(props){
        super(props);
        this.state = {
            text: this.props.text
        }
    }
    componentDidMount() {
        var stopper = this.props.text + '...';
        this.interval = setInterval(function () {
            if(this.state.text === stopper){
                this.setState({
                    text: this.props.text
                })
            } else {
                this.setState({
                    text: this.state.text + '.'
                })
            }
        }.bind(this), this.props.speed)
    }
    componentWillUnmount () {
        clearInterval(this.interval)
    }
    render(){
        return (
            <div>
                <h1>{this.state.text}</h1>
            </div>
        )
    }
}

Loading.propTypes = {
    text: PropTypes.string,
    speed: PropTypes.number,
}

Loading.defaultProps = {
    text: 'Loading',
    speed: 300
}

export default Loading
