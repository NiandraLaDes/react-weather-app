import axios from 'axios'

const api = 'c2da5dfcc7f465b339e83899284ecc8e'

var helpers = {
    getFiveDayForecast(city) {
        return axios.get('http://api.openweathermap.org/data/2.5/forecast/daily?q='+city+'-NAME&type=accurate&APPID='+api+'&cnt=5')
    }
}

export default helpers
